# YTitler
Playlist organizer for m3u files with YouTube URLs. Can also
be used as library - provides handy `Playlist` and `Scrapper`
structs.

As a library:

```
let playlist = Playlist {
    urls: vec![String::from("https://www.youtube.com/watch?v=UThGcWBIMpU")]
};
let chunks = playlist.to_chunks();
let videos = scrapper::fetch(chunks, None);

assert_eq!(videos.len(), 1);
assert_eq!(videos.first().unwrap().title.content, "WWDC 2018 Keynote — Apple");
```

As a binary

`ytitler -f my_playlist.m3u`

![preview](assets/preview.png)

## What the program is for?
If you use a video player (i.e. SMplayer) which can open
YouTube streams and keep your videos you wanna watch in
a playlist you probably has something like this:

![before](assets/playlist_1.png)

We are missing something here. The player doesn't fetch the
video titles for us. Also the durations are 00:00 until you
actually start playng the video.

YTitler can help you with this:

![after](assets/playlist_2.png)

That looks much better. You get the channel name, release date,
video title and video duration.

The URLs are fetched from the given file and processed asynchronously
in bulks of 10. You also get a nice progress bar so you know how
it's going.

For more options you can simply type in `ytitler --help`.

## What about next features.
Sure they are in progress. You can check the
[ticket list](https://gitlab.com/n1_/ytitler/issues).

## Why do you even watch YouTube videos in a desktop player?
Because YouTube playlist management sucks. And because by watching
a video in a player you get rid of ads. Yes, no ads at all.

Besides that I can filter, shuffle and use another features of
my desktop player (in my case it's SMplayer). Also I can share/sync
the playlist between my devices. VLC player on mobiles is the solution.
And again, no ads even on mobile.

## Can I contribute or vote for new features?
Absolutely! Just open up new merge request or issue.
