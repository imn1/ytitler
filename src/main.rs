// External.
#[macro_use]
extern crate log;

// Internal modules.
mod cli;

// Modules from internal library.
use ytitler::scrapper;
use ytitler::playlist::Playlist;

// Use of stuff from internal modules.
use crate::cli::args;
use crate::cli::logger;


fn main() {

    // Parse CLI arguments.
    let app_options = args::get_options();

    // Set up logging.
    logger::set_up(app_options.debug).expect("Cannot set up logging");

    let mut playlist = match Playlist::try_from(&app_options.file) {
        Ok(p) => p,
        Err(_e) => {
            error!("Couldn't load playlist from the given path.");
            return
        }
    };

    // Handle duplicities.
    playlist.remove_duplicities();
    info!("Parsed {} URL(s)", playlist.urls.len());

    // Split up into chunks.
    let chunks = playlist.to_chunks();
    info!("Chunks: {}", chunks.len());

    // Perform fetching.
    let videos = scrapper::fetch(chunks, Some(app_options.progress_bar));
    info!("{}", videos.len());

    // Determine output file.
    let output_file = if app_options.output.is_some() {
        app_options.output.unwrap()
    } else {
        app_options.file
    };

    // Save the playlist.
    match playlist.save(output_file, &videos) {
        Ok(_) => info!("Playlist has been saved."),
        Err(_) => error!("Couldn't save playlist.")
    }
}


// fn main() {

//     logger::set_up(true);

//     let playlist = Playlist {
//         urls: vec![String::from("https://www.youtube.com/watch?v=UThGcWBIMpU")]
//     };
//     let chunks = playlist.to_chunks();
//     let videos = scrapper::fetch(chunks, None);

//     assert_eq!(videos.len(), 1);
//     assert_eq!(videos.first().unwrap().title.content, "WWDC 2018 Keynote — Apple");
// }
