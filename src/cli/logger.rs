use log::{Record, Level, Metadata, SetLoggerError, LevelFilter};

struct AppLogger {
    debug: bool,
}


impl log::Log for AppLogger {
    fn enabled(&self, metadata: &Metadata) -> bool {

        // Enable levels based on self.debug flag.
        if self.debug {
            return metadata.level() <= Level::Info;
        }

        metadata.level() <= Level::Warn
    }

    fn log(&self, record: &Record) {

        if self.enabled(record.metadata()) {

            println!(
                "{} | {}:{} | {}",
                record.level(),
                record.file().unwrap(),
                record.line().unwrap(),
                record.args()
            );
        }
    }

    fn flush(&self) {}
}


pub fn set_up(debug: bool) -> Result<(), SetLoggerError> {

    let logger = AppLogger { debug };

    match log::set_boxed_logger(Box::new(logger)) {
        Ok(()) => log::set_max_level(LevelFilter::Info),
        Err(e) => return Err(e),
    };

    Ok(())
}
