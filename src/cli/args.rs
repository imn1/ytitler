use clap::{Arg, App, crate_authors, crate_version, crate_description};
use std::path::PathBuf;


pub struct AppOptions {
    pub file: PathBuf,
    pub output: Option<PathBuf>,
    pub debug: bool,
    pub progress_bar: bool,
}


pub fn get_options() -> AppOptions {

    let matches = App::new("YTitler")
        .version(crate_version!())
        .author(crate_authors!(", "))
        .about(crate_description!())
        .usage("ytitler -f <file_with_urls>")
        .arg(
            Arg::with_name("file")
                .short("f")
                .long("file")
                .value_name("FILE")
                .required(true)
                .help("File with video URLs.")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("output")
                .short("-o")
                .long("output")
                .value_name("FILE")
                .help("Output file - by default same as input (-f).")
                .takes_value(true),
        )
        .arg(Arg::with_name("debug").long("debug").help(
            "Turns on debug logs.",
        ))
        .arg(
            Arg::with_name("no-progress-bar")
                .long("no-progress-bar")
                .help("Turns off the progress bar."),
        )
        .get_matches();

    // File.
    let file = PathBuf::from(matches.value_of("file").unwrap().to_string());

    // Output.
    let output = matches.value_of_lossy("output").map(|v| {
        PathBuf::from(v.to_string())
    });

    // Debug.
    let debug = matches.is_present("debug");

    // Progress bar.
    let progress_bar = !matches.is_present("no-progress-bar");

    AppOptions {
        file,
        output,
        debug,
        progress_bar,
    }
}
